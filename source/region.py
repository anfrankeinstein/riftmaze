import copy
import slot


class Region():
    def __init__(self, card, parent):
        self.card = card
        self.parentRegion = parent
        self.regionID = None
        self.insertID = None
        self.slots = []
        self.linkRegion = [DummyRegion(self) for _ in range(card.links)]

    @staticmethod
    def assignSlots(region, slotID):
        for _ in range(region.card.slots):
            obj = slot.Slot(None)
            obj.slotID = slotID
            region.slots.append(obj)
            slotID += 1
        return slotID


class DummyRegion():
    def __init__(self, parent):
        self.regionID = None
        self.parentRegion = parent


class RegionTree():
    def __init__(self, rootCard, owner):
        self.rootRegion = Region(rootCard, owner)
        self.regionType = rootCard.regionType

        self.regionOwner = owner
        self.rootRegion.regionID = 0
        self.rootRegion.insertID = 0
        self.regionCount = 1
        self.SlotCount = Region.assignSlots(self.rootRegion, 0)

        self.linkIDMap = []
        self.insertIDMap = [0]

    def refreshIDMap(self):
        self.linkIDMap = []
        self.insertIDMap = []
        regionQuery = [self.rootRegion]
        regionCount = 0
        while len(regionQuery) != 0:
            if isinstance(regionQuery[0], DummyRegion):
                regionQuery[0].regionID = regionCount
                self.linkIDMap.append(regionQuery[0].regionID)
                regionCount += 1
                regionQuery.pop(0)
            else:
                regionQuery[0].regionID = regionCount
                self.insertIDMap.append(regionQuery[0].insertID)
                regionCount += 1
                regionQuery.extend(regionQuery[0].linkRegion)
                regionQuery.pop(0)

    def insertRegion(self, card, linkID):
        if card.regionType == self.regionType:
            self.refreshIDMap()

    def removeRegion(self, regionID):
        self.refreshIDMap()

    def swapRegion(self, fromID, toID):
        self.refreshIDMap()
