import resource
__world = None
__player = None


def gainMatter(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'Matter', param[0])


def gainMatterColorless(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterColorless', param[0])


def gainMatterCyan(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterCyan', param[0])


def gainMatterGreen(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterGreen', param[0])


def gainMatterOrange(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterOrange', param[0])


def gainMatterWhite(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterWhite', param[0])


def gainMatterGold(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterGold', param[0])


def gainMatterLime(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterLime', param[0])


def gainMatterPink(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'MatterPink', param[0])


def gainForce(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'Force', param[0])


def gainForceColorless(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'ForceColorless', param[0])


def gainForceBlack(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'ForceBlack', param[0])


def gainForceCrimson(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'ForceCrimson', param[0])


def gainForceViolet(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'ForceViolet', param[0])


def gainForceGray(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'ForceGray', param[0])


def gainForceIndigo(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'ForceIndigo', param[0])


def gainEntropy(param):
    resource.PlayerResources.gainResource(
        __player.resources, 'Entropy', param[0])
    __world.resources.loseEntropy(param[0])
