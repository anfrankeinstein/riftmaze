import card
import action
import effect
import trigger


class CardGallery():
    def __init__(self):
        self.sets = []
        self.gallery = []
        self.active = []
        self.current = []

    def loadCards(self):
        pass

    def activeSets(self):
        pass

    def inactiveSets(self):
        pass

    def generateCenterDeck(self):
        pass

    def generatePlayerDeck(self):
        pass

    def __getitem__(self, cardID: str):
        pass


def loadFunctions(module):
    gallery = {}
    for ID in dir(module):
        if '__' not in ID:
            gallery[ID] = getattr(module, ID)
    return gallery


actionGallery = loadFunctions(action)
effectGallery = loadFunctions(effect)
triggerGallery = loadFunctions(trigger)


def bindingEnv(player, world=None):
    if world != None:
        action.__world = world
        effect.__world = world
        trigger.__world = world
    action.__player = player
    effect.__player = player
    trigger.__player = player
