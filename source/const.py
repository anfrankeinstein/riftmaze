from enum import Enum, auto

GameModeFlag = [
    'StandardMode',
    'LastStandMode',
    'ArmamentRaceMode',
    'TradConflictMode',
    'MoreIsBetterMode',
    'EndlessMode',
    'StoryMode'
]

Faction = [
    'Colorless',  # stand for Nothing
    'Rainbow',  # stand for Chaos
    'Cyan',  # stand for Pride
    'Green',  # stand for Gluttony
    'Orange',  # stand for Wrath
    'White',  # stand for Sloth
    'Gold',  # stand for Greed
    'Lime',  # stand for Envy
    'Pink',  # stand for Lust
    'Black',  # stand for Order
    'Crimson',  # stand for Time
    'Violet',  # stand for Space
    'Gray',  # stand for Field
    'Indigo'  # stand for Information
]

ResourceType = [
    'Matter',
    'MatterColorless',
    'MatterRainbow'
    'MatterCyan',
    'MatterGreen',
    'MatterOrange',
    'MatterWhite',
    'MatterGold',
    'MatterLime',
    'MatterPink',
    'Force',
    'ForceColorless',
    'ForceBlack',
    'ForceCrimson',
    'ForceViolet',
    'ForceGray',
    'ForceIndigo',
    'Entropy',
    'EntropyDummy'
]

PlayerResourcesFlag = [
    'NoMoreMatter',  # player will not gain Matter
    'NoLessMatter',  # player will not lost Matter
    'InfiniteMatter'  # player has infinite Matter
    'NoMoreMatterColorless',  # player will not gain MatterColorless
    'NoLessMatterColorless',  # player will not lost MatterColorless
    'InfiniteMatterColorless'  # player has infinite MatterColorless
    'NoMoreMatterCyan',  # player will not gain MatterCyan
    'NoLessMatterCyan',  # player will not lost MatterCyan
    'InfiniteMatterCyan'  # player has infinite MatterCyan
    'NoMoreMatterGreen',  # player will not gain MatterGreen
    'NoLessMatterGreen',  # player will not lost MatterGreen
    'InfiniteMatterGreen'  # player has infinite MatterGreen
    'NoMoreMatterOrange',  # player will not gain MatterOrange
    'NoLessMatterOrange',  # player will not lost MatterOrange
    'InfiniteMatterOrange'  # player has infinite MatterOrange
    'NoMoreMatterWhite',  # player will not gain MatterWhite
    'NoLessMatterWhite',  # player will not lost MatterWhite
    'InfiniteMatterWhite'  # player has infinite MatterWhite
    'NoMoreMatterGold',  # player will not gain MatterGold
    'NoLessMatterGold',  # player will not lost MatterGold
    'InfiniteMatterGold'  # player has infinite MatterGold
    'NoMoreMatterLime',  # player will not gain MatterLime
    'NoLessMatterLime',  # player will not lost MatterLime
    'InfiniteMatterLime'  # player has infinite MatterLime
    'NoMoreMatterPink',  # player will not gain MatterPink
    'NoLessMatterPink',  # player will not lost MatterPink
    'InfiniteMatterPink'  # player has infinite MatterPink
    'NoMoreForce',  # player will not gain Force
    'NoLessForce',  # player will not lost Force
    'InfiniteForce'  # player has infinite Force
    'NoMoreForceColorless',  # player will not gain ForceColorless
    'NoLessForceColorless',  # player will not lost ForceColorless
    'InfiniteForceColorless'  # player has infinite ForceColorless
    'NoMoreForceBlack',  # player will not gain ForceBlack
    'NoLessForceBlack',  # player will not lost ForceBlack
    'InfiniteForceBlack'  # player has infinite ForceBlack
    'NoMoreForceCrimson',  # player will not gain ForceCrimson
    'NoLessForceCrimson',  # player will not lost ForceCrimson
    'InfiniteForceCrimson'  # player has infinite ForceCrimson
    'NoMoreForceViolet',  # player will not gain ForceViolet
    'NoLessForceViolet',  # player will not lost ForceViolet
    'InfiniteForceViolet'  # player has infinite ForceViolet
    'NoMoreForceGray',  # player will not gain ForceGray
    'NoLessForceGray',  # player will not lost ForceGray
    'InfiniteForceGray'  # player has infinite ForceGray
    'NoMoreForceIndigo',  # player will not gain ForceIndigo
    'NoLessForceIndigo',  # player will not lost ForceIndigo
    'InfiniteForceIndigo'  # player has infinite ForceIndigo
    'NoMoreEntropy',  # player will not gain Entropy
    'NoLessEntropy',  # player will not lost Entropy
    'InfiniteEntropy'  # player has infinite Entropy
    'NoMoreEntropyDummy',  # player will not gain EntropyDummy
    'NoLessEntropyDummy',  # player will not lost EntropyDummy
    'InfiniteEntropyDummy'  # player has infinite EntropyDummy
    'MatterAsForce',  # player can use matter as force
    'ForceAsMatter'  # player can use force as matter
]

WorldResourcesFlag = [
    'InfiniteEntropy',  # infinite entropy in world
    'EntropyLocked'  # entropy will not change
]

ResourceQuestFlag = [
    'MatterStrict',  # can only offer by matter directly
    'ForceStrict',  # can only offer by force directly
    'EntropyStrict'  # can only offer by entropy directly
]

RegionTreeType = {
    'RiftRegion',
    'CenterRegion',
    'EventRegion',
    'RegionRegion',
    'VaccumRegion',
    'DrawRegion',
    'HandRegion',
    'PlayedRegion',
    'DiscardRegion',
    'BanishRegion',
    'DefeatRegion'
}
