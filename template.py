gainTemplate = '''
def gainz(param):
    resource.PlayerResources.gainResource(__player.resources,'z',param[0])
    '''
resourceFlagTemplate = '''
    'NoMorez',#player will not gain z
    'NoLessz',# player will not lost z
    'Infinitez' # player has infinite z'''
ResourceType = ['Matter',
                'MatterColorless',
                'MatterCyan',
                'MatterGreen',
                'MatterOrange',
                'MatterWhite',
                'MatterGold',
                'MatterLime',
                'MatterPink',
                'Force',
                'ForceColorless',
                'ForceBlack',
                'ForceCrimson',
                'ForceViolet',
                'ForceGray',
                'ForceIndigo',
                'Entropy',
                'EntropyDummy']


def gen(template: str, strings):
    out = open('gencode.txt', 'w')
    for k in strings:
        out.write(template.replace('z', k))


gen(resourceFlagTemplate, ResourceType)
