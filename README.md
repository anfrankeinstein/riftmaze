# Rift Maze 裂隙谜踪

## 游戏简介
《裂隙谜踪》是一款1~4人的卡组构筑游戏（Deck Building Games)，主要创作灵感来源于暗杀神（Ascension）。玩法基本上脱胎于暗杀神，主要创新点为增加了区域机制，和专门的事件牌堆来提供更有趣的游戏体验。

## 代码简介
+ action.py 动作器的代码
+ builder.py 生成World与Player
+ card.py 卡牌定义与卡牌实例的抽象
+ const.py 一些常量，不直接使用仅作为开发人员提示
+ core-building.py core 指游戏核心内容，将被json代替
+ effect.py 效果器的代码
+ gallery.py 生成卡牌，效果器等的表
+ modifier.py 更改器的代码
+ player.py 管理玩家数据的类Player
+ region.py 承载Slot的类Region与相应的树结构
+ resource.py 游戏内资源结算类
+ serializer.py 读写本地文件的函数
+ slot.py 实际承载卡堆的类Slot
+ trigger.py 触发器的代码
+ world.py 管理游戏环境数据的类World
+ template.py 模板生成代码，用于生产一些重复代码

## RoadMap
+ -> 完成代码框架 (当前:Region)
+  完成core的卡牌定义设计
+  完成CLI接口
+  进行alpha测试