
import json


def encodeFile(plaintext: str):
    return plaintext


def decodeFile(ciphertext: str):
    return ciphertext


def saveGame(game, fileName: str):
    pass


def loadGame(fileName: str):
    pass


DefaultConfig = {
    "StandardModeWinner": "Entropy",
    "LastStandModeWinner": "Entropy",
    "LastStandModeNoMoreEntropy": False,
    "ArmamentRaceModePreparePhase": 20,
    "ArmamentRaceModeFinalPhase": 5,
    "TradeConflictModePreparePhase": 20,
    "TradeConflictModeFinalPhase": 5,
    "MoreIsBetterModeWinner": "DeckSize",
    "StoryModeDecided": "RandomAllocate",

    "PrivateBanishRegion": False,
    "PrivateDefeatRegion": False,
    "IndependentRegionDeck": False,
}


def initConfig():
    saveConfig(DefaultConfig, "config.cfg")


def saveConfig(config, fileName: str):
    out = open(fileName, 'w')
    out.write(json.dumps(config))


def loadConfig(fileName: str = 'config.cfg'):
    inFile = open(fileName)
    return json.loads(inFile.read())


if __name__ == "__main__":
    initConfig()
    print(loadConfig())
