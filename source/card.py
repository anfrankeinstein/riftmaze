class Card():
    def __init__(self):
        self.cardID = None
        self.title: str = None
        self.description: str = None
        self.comment: str = None
        self.faction: str = None
        self.size = None
    pass


class EventCard(Card):
    def __init__(self):
        Card.__init__(self)
        self.expire = None
        self.effect = None

        pass


class RegionCard(Card):
    def __init__(self):
        Card.__init__(self)
        self.slots = None
        self.regionType = None
        self.links = None
        self.cost = None
        self.need = None
        self.entropy = None

        self.effect = None
        pass


class HeroCard(Card):
    def __init__(self):
        Card.__init__(self)
        self.cost = None
        self.need = None
        self.entropy = None
        self.effect = None
        pass


class EnemyCard(Card):
    def __init__(self):
        Card.__init__(self)
        self.cost = None
        self.need = None
        self.effect = None
        pass


class ArtifactCard(Card):
    def __init__(self):
        Card.__init__(self)
        self.cost = None
        self.entropy = None
        self.effect = None
        pass


class CardInstance():
    def __init__(self, cardID):
        self.cardID = cardID
        self.instanceID = None
        self.modifier = None
    pass


class EventCardInstance(Card):
    def __init__(self, cardID):
        CardInstance.__init__(self, cardID)
        pass


class RegionCardInstance(Card):
    def __init__(self, cardID):
        CardInstance.__init__(self, cardID)
        pass


class HeroCardInstance(Card):
    def __init__(self, cardID):
        CardInstance.__init__(self, cardID)
        pass


class EnemyCardInstance(Card):
    def __init__(self, cardID):
        CardInstance.__init__(self, cardID)
        pass


class ArtifactCardInstance(Card):
    def __init__(self, cardID):
        CardInstance.__init__(self, cardID)
        pass
