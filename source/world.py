import resource


class World():
    def __init__(self):
        self.gameMode = None
        self.subGameMode = None
        self.resources = resource.WorldResources()
        self.players = []
        self.currentPlayer = -1

        self.centerRow = None
        self.centerDeck = None
        self.EventDeck = None
        self.vaccumDeck = None
        pass

    def turnPass(self):
        self.resources.turnPass()
        self.currentPlayer += 1
        if self.currentPlayer == len(self.players):
            self.roundPass()

    def roundPass(self):
        self.resources.roundPass()
        self.currentPlayer = 0

    def phaseShift(self):
        pass


class isGameOver():
    def __init__(self, world, player):
        self.world = world
        self.player = player

    def __call__(self):
        pass

    # 标准模式 standard mode - 2-4 players scramble for entropy, game over when world entropy is 0
    # sub : entropy / deck size

    # 血战到底 last stand mode - 2-4 players scramble for entropy, game over when center deck is empty
    # sub : entropy / deck size | no more entropy

    # 军备竞赛 armament race mode - 2-4 players after x rounds, give y round to produce force, most is winner
    # sub : x[10-30] | y [1-10]

    # 贸易冲突 trade conflict mode - 2-4 players after x rounds, give y round to produce matter, most is winner
    # sub : x[10-30] | y [1-10]

    # 多多益善 more is better mode - 2-4 players scramble for cards game over when region deck is empty
    # sub : types | colors | groups

    # 无尽之旅 endless mode - 1 player enjoy the game ,gameover when center deck is empty
    # no winner or win at beginning

    # 故事模式 story mode - 2-4 players,gain a story card at start, finish it to win, game over when everyone finish
    # sub : types / colors / groups | choose / ban pick / random / random choose[1-3] | by dice / by order
