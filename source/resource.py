import const


class Resource():
    def __init__(self, upper: int = None, lower: int = 0, turnLoss: int = None, roundLoss: int = None, default: int = 0):

        self.upper = upper  # upper bound of value (value<=upper)
        self.lower = lower  # lower bound of value (value>=lower)
        # guarantee lower <=upper
        if upper != None and lower != None and lower > upper:
            self.lower = self.upper
        # the value lost each turn (turnLoss=None means value reset to default after turn )
        self.turnLoss = turnLoss
        # the value lost each round (roundLoss=None means value reset to default after round )
        self.roundLoss = roundLoss
        self.default = default  # default value of resource
        self.value = self.default  # value of resource

    def __add__(self, rhs):
        if isinstance(rhs, int):
            self.value += rhs
        elif isinstance(rhs, Resource):
            self.value += rhs.value
        else:
            return self
        # guarantee value is legal
        if self.lower != None and self.value < self.lower:
            self.value = self.lower
        if self.upper != None and self.value > self.upper:
            self.value = self.upper
        return self

    def __sub__(self, rhs):
        if isinstance(rhs, int):
            self.value -= rhs
        elif isinstance(rhs, Resource):
            self.value -= rhs.value
        else:
            return self
        # guarantee value is legal
        if self.lower != None and self.value < self.lower:
            self.value = self.lower
        if self.upper != None and self.value > self.upper:
            self.value = self.upper
        return self

    @staticmethod
    def turnPass(resource):
        if resource.turnLoss == None:
            resource.value = resource.default
        else:
            resource.value -= resource.turnLoss

    @staticmethod
    def roundPass(resource):
        if resource.roundLoss == None:
            resource.value = resource.default
        else:
            resource.value -= resource.roundLoss


class BaseResources():
    def __init__(self):

        self.turnFlag = []  # flags will clear after each turn
        self.roundFlag = []  # flags will clear after each round

    def activeFlag(self, flag: const.PlayerResourcesFlag, turn: bool = True):
        # if turn is True
        if turn:
            self.turnFlag.append(flag)
        # if turn is False => round is True
        else:
            self.roundFlag.append(flag)


class WorldResources(BaseResources):
    def __init__(self):
        BaseResources.__init__(self)
        self.entropy = Resource(None, 0, 0, 0, 0)

    def loseEntropy(self, value):
        if 'EntropyLocked' not in self.turnFlag and 'InfiniteEntropy' not in self.roundFlag:
            self.entropy -= value

    def turnPass(self):
        self.turnFlag = []
        Resource.turnPass(self.entropy)

    def roundPass(self):
        self.roundFlag = []
        Resource.roundPass(self.entropy)


class PlayerResources(BaseResources):
    def __init__(self):
        BaseResources.__init__(self)
        self.resourceSet = {}
        for resource in const.ResourceType:
            self.resourceSet[resource] = Resource
        self.resourceSet['Entropy'] = Resource(None, 0, 0, 0, 0)

    def useFaction(self, value: int, faction: str):
        if 'Infinite'+faction in self.turnFlag:
            return 0
        if 'NoLess'+faction not in self.turnFlag:
            if self.resourceSet[faction].value >= value:
                self.resourceSet[faction] -= value
                return 0
            else:
                value -= self.resourceSet[faction].value
                self.resourceSet[faction].value = 0
                return value
        return 0

    def useMatter(self, value: int):
        if 'InfiniteMatter' in self.turnFlag:
            return 0
        if 'NoLessMatter' not in self.turnFlag:
            self.resourceSet['Matter'] -= value
        return 0

    def useForce(self, value: int):
        if 'InfiniteForce' in self.turnFlag:
            return 0
        if 'NoLessForce' not in self.turnFlag:
            self.resourceSet['Force'] -= value
        return 0

    def useDummy(self, value: int):
        if 'InfiniteEntropyDummy' in self.turnFlag:
            return 0
        if 'NoLessEntropyDummy' not in self.turnFlag:
            if self.resourceSet['EntropyDummy'].value >= value:
                self.resourceSet['EntropyDummy'] -= value
                return 0
            else:
                value -= self.resourceSet['EntropyDummy'].value
                self.resourceSet['EntropyDummy'].value = 0
                return value
        return 0

    def useEntropy(self, value: int):
        if 'InfiniteEntropy' in self.turnFlag:
            return 0
        if 'NoLessEntropy' not in self.turnFlag:
            self.resourceSet['Entropy'] -= value
        return 0

    @staticmethod
    def gainResource(playerResources, resourcesType: str, value: int):
        if resourcesType in playerResources:
            if 'NoMore'+resourcesType not in playerResources.turnFlag:
                playerResources.resourceSet[resourcesType] + value

    def turnPass(self):
        self.turnFlag = []
        for resource in self.resourceSet:
            Resource.turnPass(self.resourceSet[resource])

    def roundPass(self):
        self.roundFlag = []
        for resource in self.resourceSet:
            Resource.roundPass(self.resourceSet[resource])


class ResourceQuest():
    def __init__(self, faction: str, cost: dict = {}, need: dict = {}, flag=[]):
        self.faction = faction
        if self.faction in ['Colorless', 'Rainbow', 'Cyan', 'Green', 'Orange', 'White', 'Gold', 'Lime', 'Pink']:
            self.faction = 'Matter'+self.faction
        elif self.faction in ['Black', 'Crimson', 'Violet', 'Gray', 'Indigo']:
            self.faction = 'Force' + self.faction
        self.cost = cost  # the resource will decrease after success quest
        self.need = need  # the resource won't decrease after success quest
        self.flag = flag

    def isEnough(self, resources):
        res = []
        for resource in self.cost:
            function = getattr(self, '__is'+resource+'Enough')
            res.append(function(resources, self.cost))
        for resource in self.need:
            function = getattr(self, '__is'+resource+'Enough')
            res.append(function(resources, self.need))
        return list(filter(lambda x: not x, res)) == []

    def __isMatterEnough(self, resources, where):
        own = None
        if 'ForceAsMatter' not in resources.turnFlag:
            if 'InfiniteMatter' in resources.turnFlag or 'Infinite'+self.faction in resources.turnFlag:
                return True
            own = resources.resourceSet['Matter'] + \
                resources.resourceSet[self.faction]
        else:
            if 'InfiniteMatter' in resources.turnFlag or 'InfiniteForce' in resources.turnFlag or 'Infinite'+self.faction in resources.turnFlag:
                return True
            own = resources.resourceSet['Matter'] + \
                resources.resourceSet['Force'] + \
                resources.resourceSet[self.faction]
        if 'MatterStrict' in self.flag:
            if 'InfiniteMatter' in resources.turnFlag:
                return True
            own = resources.resourceSet['Matter']
        return own.value >= where['Matter']

    def __isForceEnough(self, resources, where):
        own = None
        if 'MatterAsForce' not in resources.turnFlag:
            if 'InfiniteForce' in resources.turnFlag or 'Infinite'+self.faction in resources.turnFlag:
                return True
            own = resources.resourceSet['Force'] + \
                resources.resourceSet[self.faction]
        else:
            if 'InfiniteMatter' in resources.turnFlag or 'InfiniteForce' in resources.turnFlag or 'Infinite'+self.faction in resources.turnFlag:
                return True
            own = resources.resourceSet['Matter'] + \
                resources.resourceSet['Force'] + \
                resources.resourceSet[self.faction]

        if 'ForceStrict' in self.flag:
            if 'InfiniteForce' in resources.turnFlag:
                return True
            own = resources.resourceSet['Force']
        return own.value >= where['Force']

    def __isEntropyEnough(self, resources, where):
        own = resources.resourceSet['Entropy'] + \
            resources.resourceSet['EntropyDummy']
        if 'InfiniteEntropy' in resources.turnFlag or 'InfiniteEntropyDummy' in resources.turnFlag:
            return True
        if 'EntropyStrict' in self.flag:
            own = resources.resourceSet['Entropy']
            if 'InfiniteEntropy' in resources.turnFlag:
                return True
        return own.value >= where['Entropy']

    def deal(self, resources: PlayerResources):
        value = 0
        if self.isEnough(resources):
            if 'Matter' in self.cost:
                value = self.cost['Matter']
                if 'MatterStrict' not in self.flag:
                    value = resources.useFaction(value, self.faction)
                value = resources.useMatter(value)
            if 'Force' in self.cost:
                value = self.cost['Force']
                if 'ForceStrict' not in self.flag:
                    value = resources.useFaction(value)
                value = resources.useForce(value)
            if 'Entropy' in self.cost:
                value = self.cost['Entropy']
                if 'EntropyStrict' not in self.flag:
                    value = resources.useDummy(value, self.faction)
                value = resources.useEntropy(value)
