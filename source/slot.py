import random


class Slot():
    def __init__(self, deck, maxSize=None, visibility="Top"):
        self.cards = []  # cards the slot contained
        self.deck = deck  # where the cards filling this slot from
        # int : max card size
        # None : no limit
        self.maxSize = maxSize  # max card size the slot can contain
        # 'Top': can only see top card
        # 'All' : can see each one
        # 'None' : can see no one
        self.visibility = visibility  # which cards we can see
        self.slotID = None
        self.insertID = None

    def isFull(self):
        return sum([card.size for card in self.cards]) >= self.maxSize

    def fill(self):
        if not self.isFull() and self.deck != None:
            pass
        pass

    def fillTimes(self, times):
        for _ in range(times):
            self.fill()

    def fillAll(self):
        while not self.isFull():
            self.fill()

    def clear(self):
        self.cards.clear()

    def popHead(self):
        return self.cards.pop(0)

    def popTail(self, card):
        return self.cards.pop()

    def pushHead(self, card):
        self.cards.insert(0, card)

    def pushTail(self, card):
        self.cards.append(card)

    def shuffle(self):
        random.shuffle(self.cards)

    def mergeHead(self, slot):
        slot.cards.extend(self.cards)
        self.cards = slot.cards
        slot.clear()

    def mergeTail(self, slot):
        self.cards.extend(slot.cards)
        slot.clear()

    def searchCard(self, condition):
        for index in range(len(self.cards)):
            if condition(self.cards[index]):
                return self.cards.pop(index)

    def search(self, condition, num):
        resultIndex = list(filter(lambda x: condition(
            self.cards[x]), range(len(self.cards))))
        if len(resultIndex) > num:
            resultIndex = resultIndex[:num]
        resultCards = []
        for index in resultIndex[::-1]:
            resultCards.append(self.cards.pop(index))
        return resultCards
