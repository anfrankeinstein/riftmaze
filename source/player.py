import resource


class Player():
    def __init__(self):
        self.resources = resource.PlayerResources()

        self.drawDeck = None
        self.handDeck = None
        self.discradDeck = None
        self.playDeck = None
        self.artifactDeck = None
        self.defeatDeck = None
        self.banishDeck = None

    def acquire(self):
        pass

    def defeat(self):
        pass

    def draw(self, chainID=0, slotID=0):
        pass

    def play(self, chainID=0, slotID=0):
        pass

    def discard(self, chainID=0, slotID=0):
        pass

    def turnPass(self):
        pass

    def roundPass(self):
        pass
